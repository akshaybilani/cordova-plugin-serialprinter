PhoneGap app integration
JusPay has a native android  client which can be used by Cordova applications. To get started, you first need to download the code for the plugin.

Installation
This requires cordova CLI 5.0+ (current stable v1.5.3).

Install via repo url directly (latest version)

phonegap plugin add https://bitbucket.org/akshaybilani/cordova-plugin-serialprinterplugin

or

cordova plugin add https://bitbucket.org/akshaybilani/cordova-plugin-serialprinterplugin