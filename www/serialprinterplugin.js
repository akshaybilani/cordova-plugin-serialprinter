var exec = require('cordova/exec');
var SerialPrinterPlugin = function() {};
SerialPrinterPlugin.prototype.doprint = function(action,args,success, fail) {
    exec(success, fail, "SerialPrinterPlugin",action, JSON.parse(args));
};
var SerialPrinterPlugin = new SerialPrinterPlugin();
module.exports = SerialPrinterPlugin;